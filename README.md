## About

We use microk8s and to deploy ROS 2 robotic applications

For the moment we have two applications: 

* ros-turtlebot : simulation of the robots (webots) 
* ros-cartographer : the SLAM application 

## Kubernetes installation and setup
Install and setup `microk8s` on every node :
```
sudo snap install microk8s --classic
sudo usermod -a -G microk8s $USER
sudo chown -f -R $USER ~/.kube
newgrp microk8s 
```

On the master node enable dns :
```
microk8s enable dns multus
```

Make sure your installation is healty from the master node with :
```
microk8s status 
```

## Network configuration
**WIP**

Make sure to know your network interface (`eth0`, `wlan0`...)

Use the following command :
```
ip route
```
Note your subnet and gateway, modify it in the yaml file.

To add a node do one the master node :
```
microk8s.add-node
```
Copy paste the result on the slave nodes.

Check your nodes with : 
```
microk8s.kubectl get nodes
```

## Deploy the cluster 
To deploy : 
```
microk8s.kubectl apply -f ros-deployment.yaml
```

You can verify the deployment with : 
```
microk8s.kubectl get all
```
You must have something like that : 
```
NAME                                              READY   STATUS             RESTARTS       AGE
pod/ros-cartographer-deployment-d8d57fd66-q6q8g   1/1     Running            0              29m
pod/ros-turtlebot-deployment-7f8f8c75df-xl5dz     0/1     CrashLoopBackOff   10 (94s ago)   29m
pod/ros-turtlebot-deployment-7f8f8c75df-m546p     0/1     CrashLoopBackOff   10 (90s ago)   29m

NAME                 TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.152.183.1   <none>        443/TCP   28d

NAME                                          READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/ros-cartographer-deployment   1/1     1            1           29m
deployment.apps/ros-turtlebot-deployment      0/2     2            0           29m

NAME                                                    DESIRED   CURRENT   READY   AGE
replicaset.apps/ros-cartographer-deployment-d8d57fd66   1         1         1       29m
replicaset.apps/ros-turtlebot-deployment-7f8f8c75df     2         2         0       29m
```

## See logs 
You can see the logs of each application, for the moment we have two applications : 
```
microk8s.kubectl logs --follow -l app=[app name]
```
For example : 
```
microk8s.kubectl logs --follow -l app=ros-turtlebot
```

## Stop and delete the deployment
```
microk8s.kubectl delete -f ros-demo.yaml
```

## Others

### Sources
https://ubuntu.com/blog/ros-2-on-kubernetes-a-simple-talker-and-listener-setup

https://github.com/cyberbotics/webots_ros2/wiki/SLAM-with-TurtleBot3

### Docker images
docker tag cartographer:1.0 ninria/cartographer:latest

docker build -t cartographer:1.0 .

docker run turtlebot:1.0

docker push ninria/turtlebot:latest


### Next steps

* Easily configurable network interface 
* Deploy on turtlebot 3 
* Use a distributed slam (https://github.com/d-vo/collab_orb_slam2)
* Clean the repo to make it publicly available 

